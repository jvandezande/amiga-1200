library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity amiga1200plus is
    Port(  
            -- clocks
            i_CCK_clk : in STD_LOGIC; -- 3.58 mhz clock
            i_CPU_clk : in STD_LOGIC; -- 14 mhz clock
            i_PIXEL_clk : in STD_LOGIC; -- 28 mhz clock
            -- cpu signals
            cpu_addressbus : in STD_LOGIC_VECTOR (0 TO 23);
            cpu_databus : inout STD_LOGIC_VECTOR (16 TO 31);
            ndsack0 : out STD_LOGIC; -- open collector
            ndsack1 : out STD_LOGIC; -- open collector
            nhalt : out STD_LOGIC; -- open collector
            nberr : out STD_LOGIC; -- open collector
            ncpu_busgrant : out STD_LOGIC; -- open collector
            cpu_rw : in STD_LOGIC;
            ncpu_as : in std_ulogic;
            ncpu_ds : in std_ulogic;      
            nsize0 : in STD_LOGIC; 
            nsize1 : in STD_LOGIC; 
            nfc0 : in STD_LOGIC; 
            nfc1 : in STD_LOGIC; 
            nfc2 : in STD_LOGIC; 
            ncpu_busrequest : in STD_LOGIC;
            ncpu_boss : in STD_LOGIC;
            -- CIA signals
            cia_out_strobe : out STD_LOGIC; -- use a 74HCT595 on the other end 
            cia_out_data : out STD_LOGIC; -- use a 74HCT595 on the other end
            ncia_in_strobe : out STD_LOGIC; -- use a 74HCT165 on the other end 
            cia_in_data : out STD_LOGIC; -- use a 74HCT165 on the other end
            pp_databus : inout STD_LOGIC_VECTOR (7 DOWNTO 0);
            pp_ack : in STD_LOGIC; 
            pp_busy : in STD_LOGIC; 
            pp_paperout : in STD_LOGIC; 
            nfloppy_index : in STD_LOGIC;
            nkb_data : in STD_LOGIC;
            nkb_clock : in STD_LOGIC;
            -- Budgie signals
            nras0 : out STD_LOGIC;
            nras1 : out STD_LOGIC;
            ncal_ll : out STD_LOGIC;
            ncal_lm : out STD_LOGIC;
            ncal_um : out STD_LOGIC;
            ncal_uu : out STD_LOGIC;
            nbwe : out STD_LOGIC;
            nras : in STD_LOGIC;
            ncas : in STD_LOGIC;
            nalice_dwe : in STD_LOGIC;
            dra0 : in STD_LOGIC;
            -- Gayle signals
            nrom_cs : out STD_LOGIC;
            nalice_ramen : out STD_LOGIC;
            nalice_regen : out STD_LOGIC;
            nalice_bls : out STD_LOGIC;
            nIORD : out STD_LOGIC;
            nIOWR : out STD_LOGIC;
            nint2 : out STD_LOGIC; -- open collector
            nint3 : out STD_LOGIC; -- open collector
            nint6 : out STD_LOGIC; -- open collector
            external_cs : out STD_LOGIC_VECTOR (0 TO 3);
            nxready : in STD_LOGIC;
            nwait : in STD_LOGIC;
            nalice_dbr : in STD_LOGIC;
            nIDE_irq : in STD_LOGIC;
            -- CPU Bridge signals
            ncpu_bridge_en : out STD_LOGIC;
            cpu_bridge_dir : out STD_LOGIC;
            nchip_bridge_en : out STD_LOGIC;
            chip_bridge_dir : out STD_LOGIC;
            -- other signals
            nLED : out STD_LOGIC; -- open collector
            nfire1 : out STD_LOGIC; -- is the fire1 output for Alice (from the CIA)
            I2C_scl : out STD_LOGIC;
            I2C_sda : inout STD_LOGIC;
            nreset : in std_ulogic;
            nhsync : in STD_LOGIC;
            nvsync : in STD_LOGIC;
            noption0 : in STD_LOGIC; --future special settings
            noption1 : in STD_LOGIC; --future special settings
            noption2 : in STD_LOGIC --future special settings   
        );
end amiga1200plus;

architecture Behavioral of amiga1200plus is
    signal rom_ovl: STD_LOGIC;
    signal clocks_until_dsack_cia : integer range 0 to 32 := 32;
    signal clocks_since_AS : integer range 0 to 32 := 0;
    signal clocks_since_AS_running : STD_LOGIC := '0';
    signal cia0_pra : STD_LOGIC_VECTOR (0 TO 7);
    signal cia0_prb : STD_LOGIC_VECTOR (0 TO 7);
    signal cia0_prb_ddr : STD_LOGIC_VECTOR (0 TO 7);
    signal cia1_pra : STD_LOGIC_VECTOR (0 TO 7);
    signal cia1_prb : STD_LOGIC_VECTOR (0 TO 7);
    signal pp_strobe : STD_LOGIC;
    signal dataout_shift_register : STD_LOGIC_VECTOR (0 TO 11);
    signal dataout_shift_cntr : integer range 0 to 11 := 0;
    signal datain_shift_register : STD_LOGIC_VECTOR (0 TO 9);
    signal datain_shift_cntr : integer range 0 to 9 := 0;
begin  


    -- This uses two 74HCT595 Shift Registers on the other end.
    -- 11   10   9        8     7     6     5     4     3     2   1     0
    -- ---------------------------------------------------------------------
    -- /DTR /RTS /PSTROBE /MTR  /SEL3 /SEL2 /SEL1 /SEL0 /SIDE DIR /STEP /LED
    proc_cia_data_out: process(i_PIXEL_clk, nreset)
    begin
        if nreset = '1' then --not in reset
            if i_PIXEL_clk = '1' then
                dataout_shift_cntr <= dataout_shift_cntr + 1;
                if dataout_shift_cntr = 11 then
                    dataout_shift_cntr <= 0;
                    cia_out_strobe <= '0'; --de-assert strobe
                    dataout_shift_register(0) <= cia0_pra(1); --led
                    if cia0_pra(1) = '0' then
                        nLED <= '0';
                    else
                        nLED <= 'Z';
                    end if;
                    dataout_shift_register(1 TO 8) <= cia1_prb(0 TO 7);
                    dataout_shift_register(9) <= pp_strobe;
                    dataout_shift_register(10 TO 11) <= cia0_pra(6 TO 7);
                end if;
            else
                cia_out_data <= dataout_shift_register(dataout_shift_cntr);
                if dataout_shift_cntr = 0 then        
                    cia_out_strobe <= '1'; --assert strobe
                end if;
            end if;
        else -- in reset
            dataout_shift_register <= "000000000000";
            dataout_shift_cntr <= 0;
            cia_out_strobe <= '0'; --de-assert strobe
            nLED <= 'Z';
        end if;
    end process;

    -- This uses two 74HCT165 Shift Registers on the other end.
    -- 9        8     7     6     5     4     3     2     1    0
    -- --------------------------------------------------------------
    -- /FIR1 /FIR0  /RDY /TK0  /WPRO /CHNG /CD   /CTS  /DSR    SEL
    proc_cia_data_in: process(i_PIXEL_clk, nreset)
    begin
        if i_PIXEL_clk = '1' then
            datain_shift_cntr <= datain_shift_cntr + 1;
            ncia_in_strobe <= '1'; --de-assert strobe
            if datain_shift_cntr = 9 then
                datain_shift_cntr <= 0;       
                --datain_shift_register(0) <= cia0_pra(1); --led
            end if;
        else
            cia_in_data <= datain_shift_register(datain_shift_cntr);
            if datain_shift_cntr = 0 then        
                ncia_in_strobe <= '0'; --assert strobe
            end if;
        end if;
    end process;

    proc_address_decoding: process(ncpu_as, clocks_since_AS, nalice_dbr, nreset)
    variable cia_regaddr : STD_LOGIC_VECTOR (0 TO 3);
    begin
        if nreset = '0' then
            rom_ovl <= '1';
        else
            if ncpu_as = '0' then
                -- ROM CS
                if ((cpu_addressbus >= x"0A80000") and (cpu_addressbus <= x"0B7FFFF")) or
                ((cpu_addressbus >= x"0E00000") and (cpu_addressbus <= x"0E7FFFF")) or
                ((cpu_addressbus >= x"0F80000") and (cpu_addressbus <= x"0FFFFFF")) or
                ((rom_ovl = '1') and (cpu_addressbus >= x"0000000") and (cpu_addressbus <= x"01FFFFF")) then 
                    nrom_cs <= '0'; --assert CS
                    clocks_since_AS_running <= '1'; --start counter
                    if clocks_since_AS > 2 then -- assert dsack 32 bit termination after 107 ns
                        ndsack0 <= '0';
                        ndsack1 <= '0';
                    else
                        ndsack0 <= 'Z';
                        ndsack1 <= 'Z';
                    end if;
                else
                    nrom_cs <= 'Z';
                end if;

                -- CHIP RAM
                if ((rom_ovl = '0') and (cpu_addressbus >= x"0000000") and (cpu_addressbus <= x"0200000")) then 
                    nalice_ramen <= '0';
                else
                    nalice_ramen <= 'Z';
                end if;

                -- CHIP Registers
                if ((cpu_addressbus >= x"0DFF000") and (cpu_addressbus <= x"0DFF1FF")) then 
                    nalice_regen <= '0';
                else
                    nalice_regen <= 'Z';
                end if;

                -- CIA0 CS
                -- ---------------------------------------------------------------------------
                    --  Byte    Register                  Data bits
                    -- Address    Name     7     6     5     4     3     2     1    0
                    -- ---------------------------------------------------------------------------
                    -- BFE001    pra     /FIR1 /FIR0  /RDY /TK0  /WPRO /CHNG /LED  OVL
                    -- BFE101    prb     Parallel port
                    -- BFE201    ddra    Direction for port A (BFE001);1=output (set to 0x03)
                    -- BFE301    ddrb    Direction for port B (BFE101);1=output (can be in or out)
                    -- BFE401    talo    CIAA timer A low byte (.715909 Mhz NTSC; .709379 Mhz PAL)
                    -- BFE501    tahi    CIAA timer A high byte
                    -- BFE601    tblo    CIAA timer B low byte (.715909 Mhz NTSC; .709379 Mhz PAL)
                    -- BFE701    tbhi    CIAA timer B high byte
                    -- BFE801    todlo   50/60 Hz event counter bits 7-0 (VSync or line tick)
                    -- BFE901    todmid  50/60 Hz event counter bits 15-8
                    -- BFEA01    todhi   50/60 Hz event counter bits 23-16
                    -- BFEB01            not used
                    -- BFEC01    sdr     CIAA serial data register (connected to keyboard)
                    -- BFED01    icr     CIAA interrupt control register
                    -- BFEE01    cra     CIAA control register A
                    -- BFEF01    crb     CIAA control register B

                    -- Note:  CIAA can generate interrupt INT2.
                if ((cpu_addressbus >= x"00BFE000") and (cpu_addressbus <= x"00BFEFFF")) then 
                    cia_regaddr(0 TO 3) := cpu_addressbus(8 TO 11);
                    if cia_regaddr = x"0" then --PRA
                    
                    elsif cia_regaddr = x"1" then --PRB
                    if cpu_rw = '0' then --write
                        if ncpu_ds = '0' then -- cpu data strobe asserted
                            pp_databus <= cpu_databus(16 TO 23);
                            --TODO: strobe 500 + 500 + 500 ns
                            clocks_until_dsack_cia <= 14;
                        end if;
                    else  --read
                        cpu_databus(16 TO 23) <= pp_databus;
                        clocks_until_dsack_cia <= 1;
                    end if;
                    elsif cia_regaddr = x"2" then --DDRA
                    elsif cia_regaddr = x"3" then --DDRB
                    end if;
                    clocks_since_AS_running <= '1'; --start counter
                    if clocks_since_AS > clocks_until_dsack_cia then -- assert dsack 16 bit termination
                        ndsack1 <= '0';
                    else
                        ndsack1 <= 'Z';
                    end if;
                end if;

                -- CIA1 CS
                --  ---------------------------------------------------------------------------
                --  Byte     Register                   Data bits
                --  Address     Name     7     6     5     4     3     2     1     0
                --  ---------------------------------------------------------------------------
                --  BFD000    pra     /DTR  /RTS  /CD   /CTS  /DSR   SEL   POUT  BUSY
                --  BFD100    prb     /MTR  /SEL3 /SEL2 /SEL1 /SEL0 /SIDE  DIR  /STEP
                --  BFD200    ddra    Direction for Port A (BFD000);1 = output (set to 0xFF)
                --  BFD300    ddrb    Direction for Port B (BFD100);1 = output (set to 0xFF)
                --  BFD400    talo    CIAB timer A low byte (.715909 Mhz NTSC; .709379 Mhz PAL)
                --  BFD500    tahi    CIAB timer A high byte
                --  BFD600    tblo    CIAB timer B low byte (.715909 Mhz NTSC; .709379 Mhz PAL)
                --  BFD700    tbhi    CIAB timer B high byte
                --  BFD800    todlo   Horizontal sync event counter bits 7-0
                --  BFD900    todmid  Horizontal sync event counter bits 15-8
                --  BFDA00    todhi   Horizontal sync event counter bits 23-16
                --  BFDB00            not used
                --  BFDC00    sdr     CIAB serial data register (unused)
                --  BFDD00    icr     CIAB interrupt control register
                --  BFDE00    cra     CIAB Control register A
                --  BFDF00    crb     CIAB Control register B
 
                --Note:  CIAB can generate INT6.
                if ((cpu_addressbus >= x"00BFD000") and (cpu_addressbus <= x"00BFDFFF")) then   
                    cia_regaddr(0 TO 3) := cpu_addressbus(8 TO 11);        
                    clocks_since_AS_running <= '1'; --start counter
                    if clocks_since_AS > clocks_until_dsack_cia then -- assert dsack 16 bit termination
                        ndsack1 <= '0';
                    else
                        ndsack1 <= 'Z';
                    end if;
                end if;
            else
            ndsack0 <= 'Z';
            ndsack1 <= 'Z';
            external_cs <= "0000";
            clocks_since_AS_running <= '0'; --stop counter
            cpu_databus <= "ZZZZZZZZZZZZZZZZ";
            end if;
        end if;
    end process;

    -- 1 count of the clock is 35.714 ns
    proc_clocks_since_AS: process(ncpu_as, i_PIXEL_clk)
    begin
        if ncpu_as = '1' then
            clocks_since_AS <= 0;
        else
            if clocks_since_AS_running = '1' then
                if falling_edge(i_PIXEL_clk) then
                    clocks_since_AS <= clocks_since_AS + 1;
                end if;
            end if;
        end if;
    end process;

end Behavioral;

